package main

import (
	"os"
	"io"
	"bytes"
	"sync"
	"time"
)

type Screen struct {
	Screen  io.Writer
	running bool
	kill    bool
	buffer  bytes.Buffer
	mutex   *sync.Mutex
	lines   int
}

func NewScreen() *Screen {
	return &Screen{
		Screen: os.Stdout,
		mutex:  &sync.Mutex{},
	}
}

func (s *Screen) Start() {
	s.kill = false
	go s.Listen()
}

func (s *Screen) Stop() {
	s.Flush()
	s.kill = true
}

func (s *Screen) Listen() {
	if s.running {
		return
	}
	for !s.kill {
		select {
		default:
			s.Wait()
		}
	}
	s.running = false
	return
}

func (s *Screen) Wait() {
	time.Sleep(time.Millisecond * 20)
	s.Flush()
}

func (s *Screen) Write(b []byte) (n int, err error) {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	return s.buffer.Write(b)
}

func (s *Screen) Flush() error {
	var lines int

	s.mutex.Lock()
	defer s.mutex.Unlock()

	if s.buffer.Len() == 0 {
		return nil
	}
	s.Clear()

	for _, byte := range s.buffer.Bytes() {
		if byte == '\n' {
			lines++
		}
	}
	s.lines = lines
	_, err := s.Screen.Write(s.buffer.Bytes())
	s.buffer.Reset()
	return err
}

func (s *Screen) Clear() {
}