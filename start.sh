#!/bin/bash

ls "./preset"
echo "INPUT THE PRESET FILE NAME OR \"ENTER\""
read FILENAME

if [ "$FILENAME" == "" ]; then 
  go run board.go screen.go main.go 
else
  echo "INPUT THE GENERATION NUMBER OR \"ENTER\""
  read GENERATION

  if [ "$GENERATION" == "" ]; then 
    go run board.go screen.go main.go "./preset/$FILENAME" 
  else
    go run board.go screen.go main.go "./preset/$FILENAME" "$GENERATION" 
  fi
fi
