package main

import (
	"os"
	"fmt"
	"time"
	"strconv"
)

var board *Board

func main() {
	if len(os.Args) > 1 {
		var filepath string = os.Args[1]
		board = LoadPreset(filepath)
	} else {
		board = Load()
	}

	var generation int
	if len(os.Args) > 2 {
		generation, _ = strconv.Atoi(os.Args[2])
	}

	screen := NewScreen()
	screen.Start()
	for i := 0; i != -1; i++ {
		board = board.Next()
		if generation == 0 {
			time.Sleep(time.Second / time.Duration(20))
			fmt.Fprintf(screen, "%v\n", board.Print())
		} else if generation == i {
			filepath := "./preset/dump.txt"
			board.Export(filepath)
			fmt.Println(filepath + " saved.")
			break;
		}
	}
}
