# game-of-life-go

Terminal based Conway's Game of Life. Implemented in Go.

## How to use

### Start

```shell
$ start.sh

### Manually Start

```shell
$ go build && game-of-life-go {FILENAME in preset FOLDER} {GENERATION_NUMBER}

## License
MIT