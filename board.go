package main

import (
	"fmt"
	"bytes"
	"strings"
	"strconv"
	"io/ioutil"
	"math/rand"
)

const (
	MIN_NUM_ROWS = 40
	MIN_NUM_COLS = 80
	DEFAULT_NUM_ROWS = MIN_NUM_ROWS
	DEFAULT_NUM_COLS = MIN_NUM_COLS
	
)

type Board struct {
	width  int
	height int
	
	grid  [][]int
}

func Load() *Board {
	board := newBoard(DEFAULT_NUM_COLS, DEFAULT_NUM_ROWS)
	for i := 0; i < (DEFAULT_NUM_COLS * DEFAULT_NUM_ROWS / 4); i++ {
		board.setLiving(rand.Intn(DEFAULT_NUM_COLS), 
			rand.Intn(DEFAULT_NUM_ROWS), 1)
	}
	return board
}

func LoadPreset(filepath string) *Board {
	file, err := ioutil.ReadFile(filepath)
	if err != nil {
		fmt.Println(filepath + " doesn't exist")
		return Load()
	}

	// info, _ := os.Stat(filepath)
	// mode := info.Mode()

	lines := strings.Split(string(file), "\n")
	if len(lines) > 2 {
		
		rowscols := strings.Split(lines[0], " ")
		rows, _ := strconv.Atoi(rowscols[0])
		cols, _ := strconv.Atoi(rowscols[1])
		if rows < MIN_NUM_ROWS {
			rows = MIN_NUM_ROWS
		}
		if cols < MIN_NUM_COLS {
			cols = MIN_NUM_COLS
		}

		board := newBoard(cols, rows)
		for _, line := range lines[2:] {
			if len(line) > 1 {
				rowcol := strings.Split(line, " ")
				row, _ := strconv.Atoi(rowcol[0])
				col, _ := strconv.Atoi(rowcol[1])
				board.setLiving(col, row, 1);
			}
		}

		return board
	}
 
	return 	newBoard(DEFAULT_NUM_COLS, DEFAULT_NUM_ROWS)
}

func newBoard(width, height int) *Board {
	grid := make([][]int, height)
	for cols := range grid {
		grid[cols] = make([]int, width)
	}
	return &Board{width: width, height: height, grid: grid}
}

func (board *Board) Next() *Board {
	newBoardVar := newBoard(board.width, board.height)
	for y := 0; y < board.height; y++ {
		for x := 0; x < board.width; x++ {
			newBoardVar.setLiving(x, y, board.NextLiving(x, y))
		}
	}
	return newBoardVar
}

func (board *Board) NextLiving(x, y int) int {
	livingNeighbors := board.Survivors(x, y)
	isLiving := board.getLiving(x, y) > 0
	if livingNeighbors == 3 || (livingNeighbors == 2 && isLiving) {
		return 1
	}
	return 0
}

func (board *Board) setLiving(x, y int, vitality int) {
	x += board.width
	x %= board.width
	y += board.height
	y %= board.height
	board.grid[y][x] = vitality
}

func (board *Board) getLiving(x, y int) int {
	x += board.width
	x %= board.width
	y += board.height
	y %= board.height
	return board.grid[y][x]
}

func (board *Board) Survivors(x, y int) int {
	alive := 0
	for i := -1; i <= 1; i++ {
		for j := -1; j <= 1; j++ {
			if (j != 0 || i != 0) && (board.getLiving(x+i, y+j) > 0) {
				alive++
			}
		}
	}
	return alive
}

func (board *Board) Print() string {
	var buffer bytes.Buffer
	for y := 0; y < board.height; y++ {
		for x := 0; x < board.width; x++ {
			if board.getLiving(x, y) > 0 {
				buffer.WriteString("█")
			} else {
				buffer.WriteByte(byte(' '))
			}
		}
		buffer.WriteByte('\n')
	}
	return buffer.String()
}

func (board *Board) Export(filepath string) {
	var output, rares []byte

	output = strconv.AppendInt(output, int64(board.height), 10)
	output = append(output, ' ')
	output = strconv.AppendInt(output, int64(board.width), 10)
	output = append(output, '\n')

	var count int
	for y := 0; y < board.height; y++ {
		for x := 0; x < board.width; x++ {
			if board.getLiving(x, y) > 0 {
				rares = strconv.AppendInt(rares, int64(x), 10)
				rares = append(rares, ' ')
				rares = strconv.AppendInt(rares, int64(y), 10)
				rares = append(rares, '\n')
				count++
			} 
		}
	}

	output = strconv.AppendInt(output, int64(count), 10)
	output = append(output, '\n')
	for _, rare := range rares {
		output = append(output, rare)
	}

	err := ioutil.WriteFile(filepath, output, 0644)
	if err != nil {
		fmt.Println(err)
		return
	}
}
